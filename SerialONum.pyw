import os
import glob
import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog
from pathlib import Path
from classes.Multi_listbox import ListBoxes
# from logging import getLogger
from classes.wraped_button import wraped_button as my_btn
from modules.open_explorer import open_explorer as opn_explr
from classes.jsonFileIo import JsonFileIo as jf
import tkinter.font as font
from modules.serialonum_information_window import show_information_window
from modules.selialonum_settings_window import open_setting_window
from classes.contextmenu import Contextmenu
from modules.serialonum_const import *
from modules.get_mydocument import getUserDocumemntPath
from modules.serialonum_mainwin_modules import *
import subprocess

# log = getLogger(__name__)

# 設定-元フォルダ選択

def is_edit(p,s):
    if p != s:
        startnum_edit()

def startnum_edit():
    redraw_btn.config(background='#ffc1c1')

def open_help():
    help_pdf = r'.\pdf\SerialONum説明書.pdf'
    subprocess.Popen(['start', help_pdf], shell=True)

#############################################
#############################################
# この行からスタート
json_data = jf(filename='serial_O_num_data.json')

root = tk.Tk()
init_win_size = get_win_size(json_data)
root.title(u"SerialONum")
root.iconbitmap(default=ICON)
root.geometry('+10+10')
root.geometry(init_win_size)
root.protocol('WM_DELETE_WINDOW', lambda: closing_func(json_data, root))
root.resizable(1, 1)
root.minsize(610,580)

# my_fontというフォントオブジェクトを新規に作成
font_size = get_font_size(json_data)
my_font = font.Font(family="BIZ UDゴシック", size=font_size)

#############################################

# 共通のpaddingサイズ設定
padSize_fillx = dict(expand='1', fill=tk.X)
padSize = dict(padx=5, pady=5)
padSize_fillBoth = padSize_fillx.copy()
padSize_fillBoth.update(fill=tk.BOTH, **padSize)

# 右と左の一番大きなフレーム
biggeest_frame = tk.Frame(root)
top_frame = tk.Frame(biggeest_frame)
left_bottom_frame = tk.Frame(biggeest_frame)
right_bottom_frame = tk.Frame(biggeest_frame)

#フォルダのフレーム
top_first_frame = tk.Frame(top_frame)
top_second_frame = tk.Frame(top_frame)
top_third_frame = tk.Frame(top_frame)

# 変換元・変換先フォルダ
origin_folder_buff = tk.StringVar()
target_folder_buff = tk.StringVar()

# デフォルトスタート番号を取得
str_startnum = tk.IntVar()
str_startnum.set(get_start_num(json_data))

json_data.set_data(START_NUM_HISTORY, str_startnum.get())

origin_folder_buff.set(get_path(json_data, 0))

target_folder_buff.set(get_path(json_data, 1))
# ボタンに共通のマウスオーバーイベントを設定する
button_cnf = {'mouse_over': '#e1ffff', 'cursor': 'hand2', 'bg':'#e1e1e1'}

listbox_cnf = {
    'column_num': 1, 'selectable': False, 'x_sizes': [50], 'y_size': 30, 'disabledforeground': 'black', 'labels': ['        番号 - ファイル名'], 'font':my_font}
listboxes = ListBoxes(right_bottom_frame, **listbox_cnf)

dict_for_execute = {'target_folder_buff': target_folder_buff,
                    'origin_folder_buff': origin_folder_buff, 'str_startnum': str_startnum, 'listboxes':listboxes}

variable_var = {'origin_folder_buff': origin_folder_buff,
                'target_folder_buff': target_folder_buff, 'str_startnum': str_startnum, 'font': my_font, 'listboxes':listboxes}

entry_cnf = {'relief':tk.GROOVE}
#############################################

# 設定メニューバー
menu_root = tk.Menu(root)

root.config(menu=menu_root)

menu_bar = tk.Menu(root, tearoff=0)
help_bar = tk.Menu(root, tearoff=0)
version_bar = tk.Menu(root, tearoff=0)

menu_root.add_cascade(label='メニュー', menu=menu_bar)
menu_root.add_cascade(label='ヘルプ', menu=help_bar)
menu_root.add_cascade(label='バージョン情報', menu=version_bar)

help_bar.add_command(label='ヘルプ', font=my_font, command=lambda:open_help())

menu_bar.add_command(label='設定', font=my_font, command=lambda: open_setting_window(
    root, json_data, **variable_var))
menu_bar.add_command(label='終了', font=my_font,
                     command=lambda: closing_func(json_data, root))

version_bar.add_command(label='バージョン情報', font=my_font,
                     command=lambda: show_information_window(root, my_font, json_data))

# 元データフォルダ参照ボタン
folder_btn = my_btn(top_first_frame, width=20, relief=tk.GROOVE, text='変換元フォルダ',
                    command=lambda: open_folder(start_numval, json_data, **dict_for_execute), font=my_font, **button_cnf)

# 元データフォルダ表示テキストボックス
folder_txtbox = tk.Entry(top_first_frame, font=my_font, width=40,
                         textvariable=origin_folder_buff, cursor='arrow', **entry_cnf)
# コンテキストメニューをくっつける
Contextmenu(folder_txtbox)

# 元データフォルダ配置
folder_btn.pack(side=tk.LEFT, **padSize)
folder_txtbox.pack(side=tk.LEFT, **padSize_fillBoth)

##################################

# 出力データフォルダ参照ボタン
out_folder_btn = my_btn(top_second_frame, width=20, font=my_font, relief=tk.GROOVE,
                        text='変換先フォルダ', command=lambda: out_folder_open(json_data, target_folder_buff), **button_cnf)
# 出力データフォルダ表示テキストボックス
out_folder_txtbox = tk.Entry(top_second_frame, font=my_font,
                             width=40, textvariable=target_folder_buff, cursor='arrow', **entry_cnf)

# コンテキストメニューをくっつける
Contextmenu(out_folder_txtbox)

# 出力データフォルダ配置
out_folder_btn.pack(side=tk.LEFT, **padSize)
out_folder_txtbox.pack(side=tk.LEFT, **padSize_fillBoth)

#############################################
# 大きなボタンの共通設定(ボタン共通設定を引き継いで再定義)
big_button_cnf = {'height': 3, 'width': 20, 'relief': tk.GROOVE, 'bd': 2}
big_button_cnf.update(button_cnf)

# 再表示ボタン
redraw_btn = my_btn(left_bottom_frame, font=my_font, text='再表示',
                    **big_button_cnf, command=lambda: show_list(start_numval, json_data, **dict_for_execute))

# 挿入O番号の始まり
validation = top_third_frame.register(is_edit)
num_label = tk.Label(top_third_frame, width=20, font=my_font, text='開始番号', justify='left')
start_numval = tk.Entry(top_third_frame, width=20, font=my_font, textvariable=str_startnum, validate='key', validatecommand=(validation, '%P','%s'), **entry_cnf)
num_label.pack(side=tk.LEFT, **padSize)
start_numval.pack(side=tk.LEFT, fill=tk.Y, **padSize)

# コンテキストメニューをくっつける
Contextmenu(start_numval)


spacing_label = tk.Label(left_bottom_frame, font=my_font, text='')
spacing_label.pack(anchor=tk.NW, **padSize)

# 再表示ボタン
redraw_btn.pack(anchor=tk.NW, **padSize)

# 処理実行ボタン
exec_btn = my_btn(left_bottom_frame, font=my_font, text='実行',
                  command=lambda: insert_o_num(json_data, **dict_for_execute), **big_button_cnf)
exec_btn.pack(anchor=tk.NW, **padSize)

# エクスプローラ表示ボタン
open_explorer_btn = my_btn(
    left_bottom_frame, font=my_font, text='変換元フォルダを開く', command=lambda: opn_explr(origin_folder_buff.get(), root), **big_button_cnf)
open_explorer_btn.pack(anchor=tk.NW, **padSize)

open_explorer_btn = my_btn(
    left_bottom_frame, font=my_font, text='変換先フォルダを開く', command=lambda: opn_explr(target_folder_buff.get(), root), **big_button_cnf)
open_explorer_btn.pack(anchor=tk.NW, **padSize)

#####################

close_button = my_btn(left_bottom_frame, font=my_font, text='閉じる',
                    **big_button_cnf, command=lambda: closing_func(json_data, root))
close_button.pack(side=tk.BOTTOM, anchor=tk.SW, **padSize)
#############################################

# ファイルとファイル連番の表示用リストボックス
listboxes.pack(anchor=tk.W, **padSize)

#############################################
biggeest_frame.pack(fill=tk.BOTH)
top_frame.pack(anchor=tk.NW, **padSize_fillx)

top_first_frame.pack(anchor=tk.NW, **padSize_fillx)
top_second_frame.pack(anchor=tk.NW, **padSize_fillx)
top_third_frame.pack(anchor=tk.NW, **padSize)

left_bottom_frame.pack(side=tk.LEFT, anchor=tk.NW)
right_bottom_frame.pack(side=tk.LEFT, anchor=tk.NW)
# ここでメインループエンド
root.mainloop()
##################################################
