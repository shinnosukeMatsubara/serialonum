import tkinter as tk

class wraped_button(tk.Button):
    def __init__(self, master=None, **kw):
        #self.get_cnf(**kw)

        if 'mouse_over' in kw:
            self.mouse_over = kw.pop('mouse_over', None)
        else:
            self.mouse_over = None
            
        super().__init__(master, **kw)
        self.create_widget()

    def get_cnf(self, **kw):
        if 'mouse_over' in kw:
            self.mouse_over = kw.pop('mouse_over')
        else:
            self.mouse_over = None

    def create_widget(self):
        if self.mouse_over is not None:
            self.origin_bg_color = self.cget('background')

            self.bind('<Enter>', self.onEntering)
            self.bind('<Leave>', self.onLeaving)

    def onEntering(self, event):
        ''' マウスオーバー時のイベント
        '''
        self.configure(background=self.mouse_over)

    def onLeaving(self, event):
        ''' マウスオーバー抜けたときのイベント
        '''
        self.configure(background=self.origin_bg_color)


if __name__ == "__main__":
    root = tk.Tk()
    button_cnf = {'mouse_over':'#00ffff'}
    button = wraped_button(root, text='button', **button_cnf)
    button.pack()
    root.mainloop()