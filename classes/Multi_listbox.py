import tkinter as tk
import tkinter.ttk as ttk


class ListBoxes(tk.Frame):

    def __init__(self, master, **cnf):
        ''' 初期設定

            cnf:
                column_num:列数
                selectable:True/False 選択可能かどうか
                x_sizes:各列のサイズ
                y_size:縦の長さ
                labels:項目名を設定
        '''

        super().__init__(master)
        #self.pack()
        # カスタムキーワード引数をデフォルトキーワード引数と分離する
        
        if 'column_num' in cnf:
            self.column_num = cnf.pop('column_num')
        else:
            self.column_num = 1

        if 'selectable' in cnf:
            self.selectable = cnf.pop('selectable')
        else:
            self.selectable = True
        
        self.x_sizes = []
        if 'x_sizes' in cnf:
            self.x_sizes.extend(cnf.pop('x_sizes'))

        if 'y_size' in  cnf:
            self.y_size = cnf.pop('y_size')
        else:
            self.y_size = None

        if 'labels' in cnf:
            self.labels = cnf.pop('labels')
        else:
            self.labels = None

        self.listbox_list = []

        self.create_wiget(master ,**cnf)

    def create_wiget(self, master, **cnf):
        ''' wigetを作成する 

        '''
        self.biggest_frame = tk.Frame(self)
        self.inner_frame_label = tk.Frame(self.biggest_frame)
        self.inner_frame_lisboxes = tk.Frame(self.biggest_frame)
        self.ysb = tk.Scrollbar(self.inner_frame_lisboxes, orient=tk.VERTICAL)
        self.xsb = tk.Scrollbar(self.inner_frame_lisboxes, orient=tk.HORIZONTAL)
        self.biggest_frame.pack(fill=tk.BOTH, expand=True)
        self.inner_frame_label.grid(column=0, row=0, sticky=tk.W + tk.E)
        self.inner_frame_lisboxes.grid(column=0, row=1,sticky=tk.W + tk.E)
        self.ysb.grid(column=1, row=1, sticky=tk.N + tk.S)
        self.xsb.grid(column=0, row=2, sticky=tk.W + tk.E)
        #self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        # 指定された列数分のループで描画

        for colnum in range(self.column_num):
            row_num = 0
            if colnum < len(self.x_sizes):
                x_size = self.x_sizes[colnum]
            else:
                x_size = None

            if self.labels:
                self.label = tk.Label(self.inner_frame_label, text=self.labels[colnum], pady=5, anchor='w', justify='left', **cnf)
                self.label.grid(column=colnum, row=row_num, sticky=tk.W)
                row_num += 1

            self.listbox = tk.Listbox(self.inner_frame_lisboxes, relief=tk.GROOVE, width=x_size, height=self.y_size, yscrollcommand=self.ysb.set, xscrollcommand=self.xsb.set, **cnf)
            self.listbox.grid(column=colnum, row=row_num, sticky=tk.W + tk.E)
            self.listbox.bind('<MouseWheel>', self.OnMouseWheel)
            #self.ysb.config(command=self.listbox.yview)

            # 選択できるかどうかを判断
            if not self.selectable:
                self.listbox.configure(state='disable')

            self.listbox_list.append(self.listbox)
        self.ysb.config(command=self.listboxes_yview)
        self.xsb.config(command=self.listboxes_xview)


    def listboxes_yview(self, event, third):
        ''' リストボックスにスクロールをバインド '''
        #lisindex = len(self.listbox_list)
        self.ysb.config(command=self.listbox_list[0].yview)
        
    def listboxes_xview(self, event, third):
        ''' リストボックスにスクロールをバインド '''
        #lisindex = len(self.listbox_list)
        self.xsb.config(command=self.listbox_list[0].xview)

    def OnMouseWheel(self, event):
        self.listbox.yview("scroll", event.delta,"units")
        # this prevents default bindings from firing, which
        # would end up scrolling the widget twice
        return "break"

    def insert_vals(self, index, value):
        ''' listに値を入れるメソッド

            index:何列目のlistか
            value:挿入する値
        '''
        if 0 <= index < len(self.listbox_list):
            if not self.selectable:
                self.listbox_list[index].config(state='normal')
                self.listbox_list[index].insert(tk.END ,value)
                self.listbox_list[index].config(state='disable')

            else:
                self.listbox_list[index].insert(tk.END, value)
        else:
            return False

    def delete_vals(self, index, **kw):
        ''' listの値を消すメソッド

            index:何列目のlistか
            kw:
                start:削除開始行 デフォルトは0
                end:削除終点行 デフォルトはend

        '''
        start = 0
        if 'start' in kw:
            start = kw.get('start') # 数値が入ることを暗黙の了解とする
        end = tk.END
        if 'end' in kw:
            end = kw.get('end') # 数値が入ることを暗黙の了解とする

        if 0 <= index < len(self.listbox_list):
            if not self.selectable:
                self.listbox_list[index].config(state='normal')
                self.listbox_list[index].delete(start, end)
                self.listbox_list[index].config(state='disable')

            else:
                self.listbox_list[index].insert(start, end)
        else:
            return False


if __name__ == "__main__":
    root = tk.Tk()

    cnf = {
        'column_num':2, 'selectable':False, 'x_sizes':[20,10], 'y_size':25, 'disabledforeground':'black','labels':['ファイル名', 'O番号']}
    listboxs = ListBoxes(root, **cnf)
    listboxs.pack(fill=tk.BOTH, expand=True)
    for i in range(50):
        if i % 2 == 0:
            listboxs.insert_vals(0,i)
        else:
            listboxs.insert_vals(1,i)

    root.mainloop()
    pass