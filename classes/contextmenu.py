import tkinter as tk

class Contextmenu(tk.Menu):
    ''' コンテキストメニュー

        引数：
        master:どこにコンテキストメニューをくっつけるのか
    '''
    def __init__(self, master, **kw):
        ''' コンストラクタ
        '''
        self.master = master
        self.create_wiget(master)


    def create_wiget(self, master):
        self.the_menu = tk.Menu(master, tearoff=False)
        self.the_menu.add_command(label="切り取り")
        self.the_menu.add_command(label="コピー")
        self.the_menu.add_command(label="貼り付け")

        #frame上で右クリックをしたら...
        self.master.bind('<Button-3>', self.show_menu)

    def show_menu(self,e):
        w = e.widget
        self.the_menu.entryconfigure("切り取り",
        command=lambda: w.event_generate("<<Cut>>"))
        self.the_menu.entryconfigure("コピー",
        command=lambda: w.event_generate("<<Copy>>"))
        self.the_menu.entryconfigure("貼り付け",
        command=lambda: w.event_generate("<<Paste>>"))
        self.the_menu.tk.call("tk_popup", self.the_menu, e.x_root, e.y_root)

if __name__ == "__main__":
    root = tk.Tk()
    root.geometry('200x200')
    text = tk.Entry(root)
    con = Contextmenu(text)
    text.pack()
    #con.pack()

    root.mainloop()
    pass