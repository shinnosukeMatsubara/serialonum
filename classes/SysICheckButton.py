import tkinter as tk
import tkinter.font as font

class SysICheckButton(tk.Frame):
    ''' ☑チェックボタンクラス

        このクラスにfont情報を渡すことで、☑も追随して大きくなる
        引数：
        kw：辞書
            font
    '''
    def __init__(self, master, **kw):
        ''' コンストラクタ '''
        super().__init__(master)

        if 'text' in kw:
            self.text = kw.pop('text')
        else:
            print('チェックボックスのテキストを指定してください')
            return

        if 'status' in kw:
            self.onOff = kw.pop('status')
        else:
            self.onOff = True

        if 'font' in kw:
            self.font = kw.pop('font')
        else:
            self.font = None

        self.create_widget(**kw)

    def create_widget(self, **kw):
        ''' widget描画'''
        self.__set_check_flg()
        self.checkbox = tk.Label(self, text=self.__str_check + self.text, font=self.font)
        self.checkbox.bind('<Button-1>', self.onClick)
        self.checkbox.pack()

    def onClick(self, event):
        ''' クリックイベント '''
        self.__reverse_flg()
        self.__set_check_flg()
        self.checkbox['text'] = self.__str_check + self.text

    def __reverse_flg(self):
        ''' クラス内のフラグを反転 '''

        if self.onOff:
            self.onOff = False
        else:
            self.onOff = True

    def __set_check_flg(self):
        ''' ☑か☐かを取得する '''
        if self.onOff:
            self.__str_check = '☑'
        else:
            self.__str_check = '☐'

    def set(self, flg):
        '''  '''
        if flg is bool:
            self.onOff = flg
            self.__set_check_flg()
            self.checkbox['text'] = self.__str_check + self.text
        else:
            print('受け取ったオブジェクトがbool型ではありません')
            return

    def get(self):
        '''  '''
        return self.onOff

if __name__ == "__main__":
    root = tk.Tk()
    root.geometry('300x300')
    my_font = font.Font(family="BIZ UDゴシック", size=20)
    checkbutton = SysICheckButton(root, text='チェックボタン', font=my_font)
    checkbutton.pack()
    root.mainloop()