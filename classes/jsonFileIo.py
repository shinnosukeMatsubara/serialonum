import json
import collections as cl
import os

class JsonFileIo:

	def __init__(self, **cnf):
		'''コンストラクタ'''

		if 'filename' in cnf:
			self.file_name = cnf.get("filename")
		else:
			self.file_name = 'ファイル名がセットされていない'
		self.json_data_dict = cl.OrderedDict()
		self.load_json_data()

	def write_json(self):
		'''jsonファイルへの書き込みメソッド'''
		with open(self.file_name , 'w') as json_file:
			json.dump(self.json_data_dict, json_file, indent=4, ensure_ascii=False)

	def load_json_data(self):
		''' jsonファイルを読み込む関数 '''

		if os.path.exists(self.file_name):
			with open(self.file_name,encoding='shift_jis') as json_file:
				self.json_data_dict = json.load(json_file)
		else:
			print('jsonファイルを読めませんでした\n')

	def get_data(self, key):
		''' 辞書からデータを引き出すメソッド '''

		if key in self.json_data_dict:
			return self.json_data_dict[key]
		else:
			return None

	def set_data(self, key, value):
		''' キーを指定してデータをセットするメソッド '''

		self.json_data_dict[key] = value


if __name__ == "__main__":
	pass