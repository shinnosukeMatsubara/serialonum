import tkinter as tk
from classes.jsonFileIo import JsonFileIo as jf
from classes.wraped_button import wraped_button as my_btn
from tkinter import ttk
from classes.contextmenu import Contextmenu
from classes.radio_button import MyRadioButton
import os
from modules.serialonum_const import *
from modules.serialonum_settingwin_modules import *
from classes.SysICheckButton import SysICheckButton as my_chckBtn
import tkinter.ttk as ttk
from modules.serialonum_settinwin_properties import SerialONumSetWinProperty as SWin_data

#############################################################
#############################################################

def ext_cmbbox_bind(e, pos_ext_cmbbox):
    ''' 変換元の拡張子のコンボボックスを選択したときにすべてのファイルであった時
    '''
    pre_cmbbox = e.widget
    if pre_cmbbox.get() == ALL_FILE:
        pos_ext_cmbbox.set('')
        pos_ext_cmbbox.config(state='disable')
    else:
        pos_ext_cmbbox.config(state="readonly")


def open_setting_window(root, json_data, **kw):

    data = SWin_data(json_data)
    if 'font' in kw:
        my_font = kw.get('font')


#######################################################

    if json_data.get_data(WINDOW_SIZE) is not None:
        winsize = json_data.get_data(WINDOW_SIZE)
    else:
        winsize = {WINDOW_SIZE: {SETTING_WINDOW_SIZE: '680x493'}}

    settingwin_size = winsize.get(SETTING_WINDOW_SIZE)
    setting_window_root = tk.Toplevel(root)
    setting_window_root.grab_set()
    setting_window_root.geometry(settingwin_size)
    setting_window_root.geometry('+20+20')
    setting_window_root.title('設定')
    setting_window_root.minsize(680, 493)

    setting_window_root.option_add('*TCombobox*Listbox*Font', my_font)

    # フレームの定義
    base_frame = tk.Frame(setting_window_root)

    top_frame = tk.Frame(base_frame)
    middle_frame = tk.Frame(base_frame)
    bottom_frame = tk.Frame(base_frame)

    folder_frame = tk.LabelFrame(top_frame, font=my_font, text='フォルダ')

    pre_first_frame = tk.Frame(folder_frame)

    pre_second_frame = tk.Frame(folder_frame)
    pos_first_frame = tk.Frame(folder_frame)
    pos_second_frame = tk.Frame(folder_frame)

    middle_left_frame = tk.Frame(middle_frame)
    middle_right_frame = tk.Frame(middle_frame)

    # setting_window_root.resizable(0, 0)

    # 共通のpaddingサイズ設定
    padSize = dict(padx=5, pady=5, anchor=tk.W)
    padSize_fillx = padSize.copy()
    padSize_fillx.update(expand='1', fill=tk.X)
    padSize_fillBoth = dict(padx=15, pady=5, expand='1', anchor=tk.W)
    padSize_fillBoth.update(fill=tk.BOTH)

    width_folders_left = 15

    # ボタンに共通のマウスオーバーイベントを設定する
    button_cnf = {'mouse_over': '#e1ffff',
                  'cursor': 'hand2', 'relief': tk.GROOVE, 'bg': '#e1e1e1'}

#######################################################

    # 変換元フォルダパス参照ボタン
    default_folder_btn = my_btn(pre_first_frame, width=width_folders_left, text='変換元フォルダ',
                                command=lambda: open_def_origin_folder(json_data, data.str_default_folder, setting_window_root), font=my_font, **button_cnf)

    # 変換元フォルダパス表示テキストボックス
    pre_folder_txtbox = tk.Entry(pre_first_frame, font=my_font,
                                 width=40, textvariable=data.str_default_folder, cursor='arrow')

    Contextmenu(pre_folder_txtbox)

    # 変換元フォルダ配置
    default_folder_btn.pack(side=tk.LEFT, **padSize)
    pre_folder_txtbox.pack(side=tk.LEFT, **padSize_fillBoth)

#######################################################
    cnbbox_padx = 18

    pre_ext_label = tk.Label(
        pre_second_frame, width=width_folders_left, font=my_font, text='種類:')
    pre_ext_cmbbox = ttk.Combobox(
        pre_second_frame, width=25, font=my_font, state="readonly")

    pos_ext_label = tk.Label(
        pos_second_frame, width=width_folders_left, font=my_font, text='種類:')
    pos_ext_cmbbox = ttk.Combobox(
        pos_second_frame, width=25, font=my_font, state="readonly")

    pre_ext_cmbbox['values'] = (NC_FILE, TXT_FILE, ALL_FILE)
    pre_ext_cmbbox.set(data.pre_ext)
    pre_ext_cmbbox.bind('<<ComboboxSelected>>',
                        lambda e: ext_cmbbox_bind(e, pos_ext_cmbbox))

    pre_reflesh_chckbtn = my_chckBtn(
        pre_second_frame, text='更新', font=my_font, status=data.pre_reflesh_flg)

    pre_ext_label.pack(side=tk.LEFT, **padSize)
    pre_ext_cmbbox.pack(side=tk.LEFT, padx=cnbbox_padx, pady=5)
    pre_reflesh_chckbtn.pack(side=tk.RIGHT, **padSize)
#######################################################

    # 変換先フォルダパス参照ボタン
    pos_default_folder_btn = my_btn(pos_first_frame, width=width_folders_left, text='変換先フォルダ',
                                    command=lambda: open_default_pos_folder(json_data, data.str_pos_default_folder, setting_window_root), font=my_font, **button_cnf)

    # 変換先フォルダパス表示テキストボックス
    pos_folder_txtbox = tk.Entry(pos_first_frame, font=my_font,
                                 width=40, textvariable=data.str_pos_default_folder, cursor='arrow')

    Contextmenu(pos_folder_txtbox)

    # 変換先フォルダパス配置
    pos_default_folder_btn.pack(side=tk.LEFT, **padSize)
    pos_folder_txtbox.pack(side=tk.LEFT, **padSize_fillBoth)

#######################################################

    pos_ext_cmbbox['values'] = (NC_FILE, TXT_FILE, '')
    pos_ext_cmbbox.set(data.pos_ext)
    pos_ext_cmbbox.config(state=data.pos_state)

    pos_reflesh_chckbtn = my_chckBtn(
        pos_second_frame, text='更新', font=my_font, status=data.pos_reflesh_flg)

    pos_ext_label.pack(side=tk.LEFT, **padSize)
    pos_ext_cmbbox.pack(side=tk.LEFT, padx=cnbbox_padx, pady=5)
    pos_reflesh_chckbtn.pack(side=tk.RIGHT, **padSize)
#######################################################

    # アドレス文字
    address_str_label = tk.Label(
        middle_left_frame, font=my_font, text='アドレス文字')
    address_str_label.pack(**padSize)

    address_frame = tk.Frame(middle_right_frame)

    address_txtbox = tk.Entry(
        address_frame, width=10, font=my_font)
    address_txtbox.pack(side=tk.LEFT)
    if data.str_address is not None:
        address_txtbox.insert(tk.END, data.str_address)

    program_numlen_info_label = tk.Label(
        address_frame, font=my_font, text='※大文字アルファベット１文字で入力')
    program_numlen_info_label.pack(side=tk.LEFT, **padSize)

    address_frame.pack(padx=5, anchor=tk.W)
#######################################################

    # プログラム番号桁数
    program_numlen_label = tk.Label(
        middle_left_frame, font=my_font, text='プログラム番号桁数')
    program_numlen_label.pack(**padSize)

    num_txtbox_checkbox_frame = tk.Frame(middle_right_frame)

    program_num_len_txtbox = tk.Entry(
        num_txtbox_checkbox_frame, width='10', font=my_font)
    program_num_len_txtbox.pack(side=tk.LEFT)
    if data.int_program_num_length is not None:
        program_num_len_txtbox.insert(tk.END, data.int_program_num_length)
    else:
        program_num_len_txtbox.insert(tk.END, 4)

    ignore_length_checkbutton = my_chckBtn(
        num_txtbox_checkbox_frame, font=my_font, text='桁数合わせ', status=data.bool_numfill)
    ignore_length_checkbutton.pack(side=tk.LEFT, **padSize)

    program_numlen_info_label = tk.Label(
        num_txtbox_checkbox_frame, font=my_font, text='※1～10で入力')
    program_numlen_info_label.pack(side=tk.LEFT, **padSize)

    num_txtbox_checkbox_frame.pack(padx=5, anchor=tk.W)

#######################################################

    # デフォルト開始番号
    start_num_frame = tk.Frame(middle_right_frame)

    default_start_num_label = tk.Label(
        middle_left_frame, font=my_font, text='開始番号')
    default_start_num_label.pack(**padSize)

    default_start_num_txtbox = tk.Entry(
        start_num_frame, width='10', font=my_font, textvariable=data.int_default_start_num)
    default_start_num_txtbox.pack(side=tk.LEFT)

    start_num_hist = my_chckBtn(start_num_frame, font=my_font, text='履歴', status=data.bool_startnum_hist)
    start_num_hist.pack(side=tk.LEFT, **padSize)

    start_num_frame.pack(padx=5, anchor=tk.W)
#######################################################

    # フォントサイズ
    font_size_label = tk.Label(middle_left_frame, text='フォントサイズ', font=my_font)
    font_size_label.pack(padx=5, pady=7, anchor=tk.W)

    radio_txt_value = {' 大 ': 15, '標準': 10}
    cnf = {'dict_text_value': radio_txt_value,
           'button_layout': tk.LEFT, 'font': my_font, 'pressed_val': data.int_fontsize}

    radio = MyRadioButton(middle_right_frame, **cnf)
    radio.pack(**padSize)

#######################################################

    win_close_cnf = {'radio': radio,'SW_data':data,
                     'pos_ext_cmbbox': pos_ext_cmbbox, 'pre_ext_cmbbox': pre_ext_cmbbox,
                     'str_address': address_txtbox, 'int_program_num_length': program_num_len_txtbox,
                      'setting_window_root': setting_window_root,
                     'root': root, 'font': my_font, 'start_num_hist':start_num_hist, 'ignore_length_checkbutton':ignore_length_checkbutton,
                     'pos_reflesh_chckbtn': pos_reflesh_chckbtn, 'pre_reflesh_chckbtn': pre_reflesh_chckbtn}

    win_close_cnf.update(kw)

    common_btn_cnf = {'height': 2, 'width': 10}
    common_btn_cnf.update(button_cnf)

    ok_button = my_btn(bottom_frame, font=my_font, text='OK', **common_btn_cnf,
                       command=lambda: on_ok(json_data, **win_close_cnf))

    cancel_button = my_btn(bottom_frame, font=my_font,
                           text='キャンセル', command=lambda: on_cancel(setting_window_root), **common_btn_cnf)

    apply_button = my_btn(bottom_frame, font=my_font, text='適用', **common_btn_cnf,
                          command=lambda: on_ok_apply(json_data, **win_close_cnf))

    apply_button.pack(side=tk.RIGHT, **padSize)
    cancel_button.pack(side=tk.RIGHT, **padSize)
    ok_button.pack(side=tk.RIGHT, **padSize)

#######################################################

    dict_frame_cnf = {'fill': tk.BOTH, 'expand': 1, 'padx': 5, 'pady': 5}
    dict_side_frame_cnf = {'side': tk.LEFT}
    dict_side_frame_cnf.update(dict_frame_cnf)

    base_frame.pack(**dict_frame_cnf)

    top_frame.pack(**dict_frame_cnf)
    middle_frame.pack(**dict_frame_cnf)
    bottom_frame.pack(**dict_frame_cnf)
    folder_frame.pack(**dict_frame_cnf)

    pre_first_frame.pack(anchor=tk.W, **dict_frame_cnf)
    pre_second_frame.pack(anchor=tk.W, **dict_frame_cnf)
    ttk.Separator(folder_frame, orient='horizontal').pack(
        fill=tk.X, expand=True, padx=20)
    pos_first_frame.pack(anchor=tk.W, **dict_frame_cnf)
    pos_second_frame.pack(anchor=tk.W, **dict_frame_cnf)

    middle_left_frame.pack(side=tk.LEFT, **padSize)
    middle_right_frame.pack(side=tk.LEFT, **padSize)

    bottom_frame.pack(**dict_frame_cnf)

#######################################################

    setting_window_root.mainloop()
