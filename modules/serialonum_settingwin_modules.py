from modules.serialonum_const import *
from tkinter import messagebox as msgbox
import os
from modules.get_mydocument import getUserDocumemntPath
from pathlib import Path
from tkinter import filedialog
from modules.serialonum_mainwin_modules import show_list
import re

def judge_win_size(json_data):
    '''' フォントサイズから判断して、ウィンドウサイズを決定 '''

    main_win_size = '650x500'
    setting_win_size = '500x300'
    win_size_dict = {MAIN_WINDOW_SIZE: main_win_size,
                     SETTING_WINDOW_SIZE: setting_win_size}

    if json_data.get_data(FONT_SIZE) is not None:
        fontsize = json_data.get_data(FONT_SIZE)
        if fontsize == 15:  # フォントサイズ大の時
            win_size_dict[MAIN_WINDOW_SIZE] = '1625x900'
            win_size_dict[SETTING_WINDOW_SIZE] = '1700x850'
            json_data.set_data(WINDOW_SIZE, win_size_dict)
        elif fontsize == 10:  # フォントサイズ小の時
            win_size_dict[MAIN_WINDOW_SIZE] = '610x580'
            win_size_dict[SETTING_WINDOW_SIZE] = '680x493'
            json_data.set_data(WINDOW_SIZE, win_size_dict)


def get_window_size(json_data):
    ''' jsonクラスからウィンドウサイズの辞書データを取り出す

        メインウィンドウサイズ
        設定ウィンドウサイズ
        が入っている
    '''

    winsize_dict = {}

    if json_data.get_data(WINDOW_SIZE) is not None:
        winsize_dict = json_data.get_data(WINDOW_SIZE)
    else:
        return None

    return winsize_dict


def on_ok(json_data, **cnf):
    ''' OKボタンが押されたとき

    '''

    if on_ok_apply(json_data, **cnf) is False:
        return

    # 設定ウィンドウクローズ
    if 'setting_window_root' in cnf:
        root = cnf.get('setting_window_root')
        root.destroy()

def set_mainwin_default_hist(json_data, **cnf):
    ''' 元フォルダ、先フォルダ、開始番号を反映する '''

    if 'SW_data' in cnf:
        data = cnf.get('SW_data')

    #メイン画面のvariable「元フォルダ、先フォルダ、開始番号」
    if 'origin_folder_buff' in cnf:
        origin_folder_buff = cnf.get('origin_folder_buff')

    if 'target_folder_buff' in cnf:
        target_folder_buff = cnf.get('target_folder_buff')

    if 'str_startnum' in cnf:
        str_startnum = cnf.get('str_startnum')

    #更新チェックがあれば
    if json_data.get_data(PRE_REFLESH):
        #デフォルトフォルダがあれば
        default = json_data.get_data(DEFAULT_FOLDER)
        if default is not None and default != '':
            #デフォルトフォルダを反映する
            origin_folder_buff.set(default)
            #ここにlistbox再表示処理を入れる
            list_rewrite(json_data, **cnf)

    if json_data.get_data(POS_REFLESH):
        #デフォルトフォルダがあれば
        pos_default = json_data.get_data(DEFAULT_POS_FOLDER)
        if pos_default is not None and pos_default != '':
            #デフォルトフォルダを反映する
            target_folder_buff.set(pos_default)


    if json_data.get_data(BOOL_STARTNUM_HISTORY):
        hist_num = json_data.get_data(START_NUM_HISTORY)
        if hist_num is not None and hist_num !='':
            str_startnum.set(hist_num)

def list_rewrite(json_data, **cnf):
    ''' listboxes を再表示する処理

    show_list(start_numval, listboxes, json_data=None, **kw):
    '''
    if 'str_startnum' in cnf:
        str_startnum = cnf.get('str_startnum')

    show_list(str_startnum, json_data, **cnf)

def on_ok_apply(json_data, **cnf):
    ''' 閉じるときの関数を修正

        OKボタンが押されたときと
        適用ボタンが押されたとき
            cnf = {'radio': radio,
                     'pos_ext_cmbbox': pos_ext_cmbbox, 'pre_ext_cmbbox': pre_ext_cmbbox,
                     'str_address': address_txtbox, 'int_program_num_length': program_num_len_txtbox,
                     ' , 'setting_window_root': setting_window_root, 'root': root, 'font': my_font}
    '''
    if 'SW_data' in cnf:
        data = cnf.get('SW_data')

    if 'setting_window_root' in cnf:
        setting_window_root = cnf.get('setting_window_root')

    #ここからバリデーション
    if 'str_address' in cnf:
        address_txtbox = cnf.get('str_address').get()
        address_validate_flg = address_char_validation(address_txtbox)

        if address_validate_flg is False:
            msgbox.showerror('エラー','アドレス文字は大文字のアルファベット１文字で入力してください。', parent=setting_window_root)
            return False

    if 'int_program_num_length' in cnf:
        program_num_len_txtbox = cnf.get('int_program_num_length').get()
        numlen_validate_flg = num_limit_one_to_ten(program_num_len_txtbox)

        if numlen_validate_flg is False:
            msgbox.showerror('エラー','桁数は1~10の間で入力してください。', parent=setting_window_root)
            return False

    if not data.save_json_settings(json_data, **cnf):
        return

    # ウィンドウサイズをフォントサイズに従って設定
    judge_win_size(json_data)

    ############### json 書き出し後の後処理 ######################

    #履歴・デフォルトをjsondataから入れこむ処理
    set_mainwin_default_hist(json_data, **cnf)

    int_origin_fontsize = data.int_origin_fontsize.get()
    int_fontsize = json_data.get_data(FONT_SIZE)
    if int_fontsize == int_origin_fontsize:
        #ウィンドウサイズの履歴を設定する。
        win_size = get_window_size(json_data)
        if win_size is None:
            return
        else:
            win_height = setting_window_root.winfo_height()
            win_width = setting_window_root.winfo_width()
            setting_win_size = str(win_width) + 'x' + str(win_height)
            win_size[SETTING_WINDOW_SIZE] = setting_win_size
            json_data.set_data(WINDOW_SIZE, win_size)
            return
    else:
        # フォントサイズの再設定
        if 'font' in cnf:
            my_font = cnf.get('font')
            if json_data.get_data(FONT_SIZE) is not None:
                my_font.configure(size=json_data.get_data(FONT_SIZE))

        # ウィンドウサイズ再表示
        if 'root' in cnf:
            root = cnf.get('root')
            if json_data.get_data(WINDOW_SIZE) is not None:
                win_size = json_data.get_data(WINDOW_SIZE)
                root.geometry(win_size.get(MAIN_WINDOW_SIZE))

        if 'setting_window_root' in cnf:
            setting_window_root = cnf.get('setting_window_root')
            if json_data.get_data(WINDOW_SIZE) is not None:
                win_size = json_data.get_data(WINDOW_SIZE)
                setting_window_root.geometry(win_size.get(SETTING_WINDOW_SIZE))


def open_def_origin_folder(json_data, str_default_folder, setting_window_root):
    ''' 元データフォルダを開く

    '''
    
    folder_txtbox_val = str_default_folder.get()
    myDocument = getUserDocumemntPath()

    if folder_txtbox_val is not None:
        if folder_txtbox_val == '':
            folder_txtbox_val =myDocument
    else:
        folder_txtbox_val = myDocument

    folder = filedialog.askdirectory(initialdir=folder_txtbox_val, parent=setting_window_root)
    json_data.set_data(DEFAULT_FOLDER, folder)

    if folder:
        str_default_folder.set(folder)

def open_default_pos_folder(json_data, str_pos_default_folder, setting_window_root):
    ''' 元データフォルダを開く
    '''

    folder_txtbox_val = str_pos_default_folder.get()
    myDocument = getUserDocumemntPath()

    if folder_txtbox_val is not None:
        if folder_txtbox_val == '':
            folder_txtbox_val = myDocument
    else:
        folder_txtbox_val = myDocument

    folder = filedialog.askdirectory(initialdir=folder_txtbox_val, parent=setting_window_root)
    json_data.set_data(DEFAULT_FOLDER, folder)

    if folder:
        str_pos_default_folder.set(folder)

###############################################

# バリデーションを実装


def num_limit_one_to_ten(str):
    ''' バリデーション：プログラム番号桁数

        1~10の数値と空値のみ許す
    '''
    if len(str) != 0:
        if str.isdigit():
            num_range = int(str)
            if num_range >= 1 and num_range <= 10:
                return True
            else:
                return False
        else:
            return False
    else:
        return True


def address_char_validation(str):
    ''' バリデーション：アドレス文字

        大文字のアルファベット文字のみ許可
    '''
    if len(str) != 0:
        if len(str) == (1 or 0):
            if not str.isdigit():
                if str == str.upper():
                    p = re.compile('[A-Z]')
                    if p.search(str) is not None:
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
    else:
        return True


def on_cancel(setting_window_root):
    ''' キャンセルボタン押下時

    '''
    bool_yesno = msgbox.askyesno('確認', '設定を保存せずに閉じます', parent=setting_window_root)
    if bool_yesno:
        setting_window_root.destroy()
    else:
        return

if __name__ == "__main__":
    msgbox.showinfo('はい', 'このファイルはSerialONum.pywから呼んでください')