import tkinter as tk
from modules.serialonum_const import *
from classes.jsonFileIo import JsonFileIo
import os
from tkinter import messagebox as msgbox


class SerialONumSetWinProperty():
    ''' jsonから取り出し、使うデータの形に変換し、保存の際の変換も受け持つ'''

    def __init__(self, json):
        self.str_default_folder = tk.StringVar()
        self.str_pos_default_folder = tk.StringVar()
        self.bool_startnum_hist = tk.BooleanVar()
        self.int_default_start_num = tk.IntVar()
        self.int_origin_fontsize = tk.IntVar()
        self.int_origin_fontsize.set(10)
        self.int_fontsize = 10

        # JSONから設定を取得
        if json.get_data(DEFAULT_FOLDER) is not None:
            self.str_default_folder.set(json.get_data(DEFAULT_FOLDER))

        if json.get_data(PRE_EXT) is not None:
            self.pre_ext = self.conv_ext_str(json.get_data(PRE_EXT))
        else:
            self.pre_ext = NC_FILE

        if json.get_data(DEFAULT_POS_FOLDER) is not None:
            self.str_pos_default_folder.set(json.get_data(DEFAULT_POS_FOLDER))

        # 変換元のファイル種別がすべてのファイルであった場合、変換後のファイル種類を選択できないようにする
        if json.get_data(POS_EXT) is not None:
            if self.pre_ext == ALL_FILE:
                self.pos_ext = ''
                self.pos_state = 'disable'
            else:
                self.pos_ext = self.conv_ext_str(json.get_data(POS_EXT))
                self.pos_state = 'readonly'
        else:
            self.pos_state = 'readonly'
            self.pos_ext = NC_FILE

        if json.get_data(FONT_SIZE) is not None:
            self.int_fontsize = json.get_data(FONT_SIZE)
            self.int_origin_fontsize.set(self.int_fontsize)

        if json.get_data(ADDRESS_STR) is not None:
            self.str_address = json.get_data(ADDRESS_STR)
        else:
            self.str_address = 'O'

        if json.get_data(PROGRAM_NUM_LENGTH) is not None:
            self.int_program_num_length = json.get_data(PROGRAM_NUM_LENGTH)
        else:
            self.int_program_num_length = '4'

        if json.get_data(BOOL_ZERO_FILL) is not None:
            self.bool_numfill = json.get_data(BOOL_ZERO_FILL)
        else:
            self.bool_numfill = False

        if json.get_data(DEFAULT_START_NUM) is not None:
            self.int_default_start_num.set(json.get_data(DEFAULT_START_NUM))
        else:
            self.int_default_start_num.set(1000)

        #元フォルダの更新チェックボックス
        if json.get_data(PRE_REFLESH) is not None:
            self.pre_reflesh_flg = json.get_data(PRE_REFLESH)
        else:
            self.pre_reflesh_flg = False

        #先フォルダの更新チェックボックス
        if json.get_data(POS_REFLESH) is not None:
            self.pos_reflesh_flg = json.get_data(POS_REFLESH)
        else:
            self.pos_reflesh_flg = False

        #開始番号を履歴で表示するかどうかチェックボックス
        if json.get_data(BOOL_STARTNUM_HISTORY) is not None:
            self.bool_startnum_hist = json.get_data(BOOL_STARTNUM_HISTORY)
        else:
            self.bool_startnum_hist = False

    def conv_ext_str(self, ext):
        ''' jsonから取得した拡張子文字列を定数値に変更する '''
        if ext == '.nc' or ext == '':
            ret = NC_FILE
        elif ext == '.*':
            ret = ALL_FILE
        elif ext == '.txt':
            ret = TXT_FILE
        else:
            print('入るはずのない拡張子が入れられています。')
            ret = None

        return ret
        
    def save_json_settings(self, json_data, **cnf):
        ''' jsonのデータを書き出し、セーブする

            フォルダの存在チェックと作成もここから行う
            引数：cnf 辞書データ
            radio, pos_ext_cmbbox, pre_ext_cmbbox, address_txtbox,
            program_num_len_txtbox, setting_window_root(設定画面), root(設定画面の親), my_font

        '''

        root_win = cnf.get('setting_window_root')
        origin_folder = self.str_default_folder.get()

        target_folder = self.str_pos_default_folder.get()

        if not self.check_and_make_dir(origin_folder, root_win):
            return False

        if not self.check_and_make_dir(target_folder, root_win):
            return False

        #変換元・変換先の拡張子を変換する
        pre_ext = cnf.get('pre_ext_cmbbox').get()
        if pre_ext == NC_FILE:
            pre_ext = '.nc'
        elif pre_ext == TXT_FILE:
            pre_ext = '.txt'
        elif pre_ext == ALL_FILE:
            pre_ext = '.*'

        pos_ext = cnf.get('pos_ext_cmbbox').get()
        if pos_ext == NC_FILE:
            pos_ext = '.nc'
        elif pos_ext == TXT_FILE:
            pos_ext = '.txt'
        elif pos_ext == '':
            pos_ext = '.*'

        # デフォルトの変換元フォルダ

        json_data.set_data(DEFAULT_FOLDER, self.str_default_folder.get())

        # 基準の元フォルダを即時反映するかどうか
        if 'pre_reflesh_chckbtn' in cnf:
            json_data.set_data(PRE_REFLESH, cnf.get('pre_reflesh_chckbtn').get())

        # 基準の先フォルダを即時反映するかどうか
        if 'pos_reflesh_chckbtn' in cnf:
            json_data.set_data(POS_REFLESH, cnf.get('pos_reflesh_chckbtn').get())

        # 変換元の拡張子
        if 'pre_ext_cmbbox' in cnf:
            json_data.set_data(PRE_EXT, pre_ext)

        # デフォルトの変換先フォルダ
        json_data.set_data(DEFAULT_POS_FOLDER, self.str_pos_default_folder.get())

        # 変換先の拡張子
        if 'pos_ext_cmbbox' in cnf:
            json_data.set_data(POS_EXT, pos_ext)

        # フォントサイズ
        if 'radio' in cnf:
            json_data.set_data(FONT_SIZE, cnf.get('radio').pressed_val)

        # アドレス文字
        if 'str_address' in cnf:
            json_data.set_data(ADDRESS_STR, cnf.get('str_address').get())

        # プログラム番号桁数
        if 'int_program_num_length' in cnf:
            json_data.set_data(PROGRAM_NUM_LENGTH, cnf.get(
                'int_program_num_length').get())

        # デフォルトスタート番号
        json_data.set_data(DEFAULT_START_NUM, self.int_default_start_num.get())

        #プログラム番号桁数を無視するかどうか
        if 'ignore_length_checkbutton' in cnf:
            json_data.set_data(BOOL_ZERO_FILL, cnf.get('ignore_length_checkbutton').get())

        #開始番号を履歴で表示するかどうかのフラグ
        if 'start_num_hist' in cnf:
            json_data.set_data(BOOL_STARTNUM_HISTORY, cnf.get('start_num_hist').get())

        json_data.write_json()

        return True


    def check_and_make_dir(self, dir, parent):
        ''' フォルダの存在を確認して、なければメッセージボックスを出して作成するか聞く最終的に存在OKならTrueを返す

            引数：
            dir:フォルダ文字列
            parent:親ウィンドウ
        '''
        if dir == '':
            return True

        if not os.path.isdir(dir):
            answer = msgbox.askyesno('確認', 'フォルダ' + dir + 'を作成しますか？', parent=parent)
            if answer:
                os.makedirs(dir, exist_ok=True)
                return True
            else:
                return False #フォルダない∧作成しない
        else:
            return True #フォルダが存在する