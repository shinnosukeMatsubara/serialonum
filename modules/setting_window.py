import tkinter as tk
from classes.jsonSettings import JsonSettings

NNUMSERIAL ='N番号は連番か？'
OFILESPLIT = 'O番号ごとにファイルを分割するか？'
EXT = '拡張子名'

def on_closing(jsonIo, **close_val_dist):
    ''' 閉じるときの関数
    '''
    if 'bool_serialnum' in close_val_dist:
        Bool_serialnum = close_val_dist.get('bool_serialnum')
    jsondict = {NNUMSERIAL:Bool_serialnum.get()}

    if 'bool_fileOsprit' in close_val_dist:
        Bool_OnumSeparate = close_val_dist.get('bool_fileOsprit')
    jsondict[OFILESPLIT] = Bool_OnumSeparate.get()


    if 'ext' in close_val_dist:
        txt_ext = close_val_dist.get('ext')
        if '.' in txt_ext.get():
            jsondict[EXT] = txt_ext.get()
        else:
            jsondict[EXT] = '.' + txt_ext.get()
    

    jsonIo.addData(**jsondict)
    jsonIo.save_json_data()

    #閉じる
    if 'root' in close_val_dist:
        root = close_val_dist.pop('root')
        root.destroy()
    pass

#設定ウィンドウの中身
def open_setting_window(root, jsonIo):


    bool_serialnum = tk.BooleanVar()
    bool_fileOsprit = tk.BooleanVar()
    txt_ext = tk.StringVar()
    dict_json = jsonIo.json_data_dict

    if NNUMSERIAL in dict_json:
        bool_serialnum.set(dict_json.get(NNUMSERIAL))
    
    if OFILESPLIT in dict_json:
        bool_fileOsprit.set(dict_json.get(OFILESPLIT))
    
    if EXT in dict_json:
        txt_ext.set(dict_json.get(EXT))
    
    
    close_val_dist = {}
    setting_window_root =  tk.Toplevel(root)
    dict = {'root':setting_window_root, 'bool_serialnum':bool_serialnum, 'bool_fileOsprit':bool_fileOsprit, 'ext':txt_ext}
    close_val_dist.update(dict)
    setting_window_root.grab_set()

    setting_window_root.geometry('230x140')
    setting_window_root.title('設定')
    setting_window_root.resizable(0,0)



    #シーケンス番号を連番にするか
    seq_frame = tk.Frame(setting_window_root)
    seq_check_box = tk.Checkbutton(seq_frame, text='シーケンス番号を連番にする', variable=bool_serialnum)
    seq_check_box.pack(side=tk.LEFT, padx=2, pady=2)
    seq_frame.pack(anchor=tk.W, padx=5, pady=5)

    #ファイルをO番号ごとに分割するか
    file_sprit_frame = tk.Frame(setting_window_root)
    file_sprit_checkbox = tk.Checkbutton(file_sprit_frame, text='ファイルをO番号ごとに分割する', variable=bool_fileOsprit)
    file_sprit_checkbox.pack(side=tk.LEFT, padx=2, pady=2)
    file_sprit_frame.pack(anchor=tk.W, padx=5, pady=5)

    #拡張子名

    ext_frame = tk.Frame(setting_window_root)
    ext_label = tk.Label(ext_frame, text='拡張子名')
    ext_label.pack(side=tk.LEFT, padx=2, pady=2)
    ext_txtbox = tk.Entry(ext_frame, textvariable=txt_ext)
    ext_txtbox.pack(side=tk.LEFT, padx=2, pady=2)
    ext_frame.pack(anchor=tk.W, padx=5, pady=5)

    setting_window_root.protocol('WM_DELETE_WINDOW', lambda: on_closing(jsonIo, **close_val_dist))


    setting_window_root.mainloop()
