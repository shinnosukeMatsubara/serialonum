from modules.serialonum_const import *
from modules.get_mydocument import getUserDocumemntPath
from tkinter import messagebox
from pathlib import Path
from tkinter import filedialog
import glob
import os

def open_folder(start_numval, json_data, **kw):
    ''' 元データフォルダを開く '''

    if 'origin_folder_buff' in kw:
        origin_folder_buff = kw.get('origin_folder_buff')
        folder_txtbox_val = origin_folder_buff.get()

    myDocument = getUserDocumemntPath()

    if folder_txtbox_val is not None and folder_txtbox_val != '':
        p_dir = Path(folder_txtbox_val)
        if not p_dir.is_dir():
            messagebox.showerror(
                'エラー', '入力されているフォルダは存在しません。\nマイドキュメントフォルダを表示します。')
            folder_txtbox_val = myDocument
    else:
        folder_txtbox_val = myDocument

    folder = filedialog.askdirectory(initialdir=folder_txtbox_val)

    if folder is not None and folder != '':
        origin_folder_buff.set(folder)
        json_data.set_data(ORIGINAL_FOLDER_HISTORY, folder)
        show_list(start_numval, json_data, **kw)


def folder_ext_sorted(folder, json_data):
    ''' フォルダ内のファイルリストを取得

        ソートする。指定された拡張子にしたがって取得
        返り値：False　か　List？
    '''
    ext = json_data.get_data(PRE_EXT)
    if ext is None or ext == '':
        ext = '.nc'

    p_path = Path(folder)
    if p_path.exists():
        files = sorted(glob.glob(folder + '/*' + ext))

    return files


def show_list(start_numval, json_data=None, **kw):
    ''' リストボックスの表示を更新する処理
        kw = {'target_folder_buff': target_folder_buff, 'origin_folder_buff': origin_folder_buff, 'str_startnum': str_startnum}
    '''
    if json_data is not None:
        ret_dict_or_bool = check_startnum_originfolder(json_data, **kw)
    else:
        ret_dict_or_bool = False

    if ret_dict_or_bool is None:
        return
    elif ret_dict_or_bool is not None and ret_dict_or_bool is not False:
        if 'start_num' in ret_dict_or_bool:
            start_num = ret_dict_or_bool.get('start_num')

        if 'origin_folder' in ret_dict_or_bool:
            origin_folder = ret_dict_or_bool.get('origin_folder')
    else:
        return

    listboxes = kw.get('listboxes', None)

    count = int(start_num)
    files = []

    files = folder_ext_sorted(origin_folder, json_data)

    if len(files) == 0:
        messagebox.showerror('エラー', '処理対象となるファイルが存在しません。')

    check_max = len(files) + count
    if check_file_count(json_data, check_max) is False:
        return

    addr_str = json_data.get_data(ADDRESS_STR)
    if addr_str is None or addr_str == '':
        addr_str = ''

    zero_fill = json_data.get_data(BOOL_ZERO_FILL)
    if listboxes is not None:
        listboxes.delete_vals(0)

    if files is not None:
        counter = int(start_numval.get())
        for index, file in enumerate(files):
            filename = os.path.basename(file)
            
            num_str = counter + index
            if zero_fill is not None and zero_fill is not False:
                num_str = formatted_count_str(json_data, num_str)
            addr_num = '{:>12}'.format(addr_str + str(num_str))
            show_str = '{} - {}'.format(addr_num, filename)
            if listboxes is not None:
                listboxes.insert_vals(0, show_str)

def out_folder_open(json_data, target_folder_buff):
    ''' 出力先フォルダ選択処理 '''

    myDocument = getUserDocumemntPath()
    folder_txtbox_val = target_folder_buff.get()

    if folder_txtbox_val is not None and folder_txtbox_val != '':
        p_dir = Path(folder_txtbox_val)
        if not p_dir.is_dir():
            messagebox.showerror(
                'エラー', '入力されているフォルダは存在しません。\nマイドキュメントフォルダを表示します。')
            folder_txtbox_val = myDocument
    else:
        folder_txtbox_val = myDocument

    folder = filedialog.askdirectory(initialdir=folder_txtbox_val)

    if folder is not None and folder != '':
        target_folder_buff.set(folder)
        json_data.set_data(TARGET_FOLDER_HISTORY, folder)


def get_address_str(json_data):
    ''' アドレス文字をjsonから設定する '''

    address_str = json_data.get_data(ADDRESS_STR)
    if address_str is None:
        address_str = 'O'
    return address_str


def formatted_count_str(json_data, count):
    ''' 指定桁数でフォーマットしたカウンターを入れる '''

    num_len = json_data.get_data(PROGRAM_NUM_LENGTH)
    if num_len is not None:
        pre_form = '{:0=' + str(num_len) + '}'
        str_count = pre_form.format(count)
    else:
        str_count = '{:0=5}'.format(count)
    return str_count


def check_startnum_originfolder(json_data, **kw):
    ''' 開始番号と処理元フォルダのチェックをする

        引数：共通のjsonのデーター、開始番号と変換元フォルダのtk.variable
        返り値：チェック結果に問題なければ
        dict ： start_numとorigin_folder
        問題あればFalse
    '''

    if 'str_startnum' in kw:
        start_num = kw.get('str_startnum').get()
        if start_num is None or start_num == '':
            messagebox.showerror('エラー', '開始番号が入力されていません。')
            return False

    if 'origin_folder_buff' in kw:
        origin_folder = kw.get('origin_folder_buff').get()
        if origin_folder is None or origin_folder == '':
            messagebox.showerror('エラー', '処理対象のフォルダを選択してください。')
            return False
        else:
            validate_dir = Path(origin_folder)
            if not validate_dir.is_dir():
                messagebox.showerror('エラー', '指定されているフォルダは存在しません。')
                return False

    ret_dict = {'start_num': start_num, 'origin_folder': origin_folder}
    return ret_dict


def check_file_count(json_data, check_max):
    ''' 指定桁数がある場合にファイルにカウント数を付けた際にオーバーしないかチェックする
    '''
    bool_zero_fill = json_data.get_data(BOOL_ZERO_FILL)
    if bool_zero_fill is not None:
        if bool_zero_fill:
            length = int(json_data.get_data(PROGRAM_NUM_LENGTH))
            if length is not None and length != '':
                if length < len(str(check_max)):
                    messagebox.showerror(
                        'エラー', '合計桁数が桁数の制限を超えてしまいます。\n入力値を確認してください。')
                    return False
                else:
                    return True
            else:
                messagebox.showerror('エラー', '桁合わせをする設定になっていますが、桁数が設定されていません。')
                return False
        else:
            return True
    else:
        return True
############### O番号挿入処理本体 ###################


def insert_o_num(json_data, **kw):
    ''' O番号挿入処理本体

    実際には多数のデータが使用される


    '''

    ret_dict_or_bool = check_startnum_originfolder(json_data, **kw)

    if not ret_dict_or_bool:
        return
    else:
        start_num = ret_dict_or_bool.get('start_num')
        origin_folder = ret_dict_or_bool.get('origin_folder')

    if 'target_folder_buff' in kw:
        target_folder = kw.get('target_folder_buff').get()
        if target_folder is None or target_folder == '':
            messagebox.showerror('エラー', '出力先フォルダを選択してください。')
            return
        else:
            varidate_targetdir = Path(target_folder)
            if not varidate_targetdir.is_dir():
                messagebox.showerror('エラー', '指定されているフォルダは存在しません。')
                return

    count = int(start_num)
    files = []

    files = folder_ext_sorted(origin_folder, json_data)

    if len(files) == 0:
        messagebox.showerror('エラー', '処理対象となるファイルが存在しません。')
        return

    check_max = len(files) + count
    if not check_file_count(json_data, check_max):
        return

    # オプションの設定
    address_str = get_address_str(json_data)
    ext = json_data.get_data(POS_EXT)
    if ext is None or ext == '':
        ext = '.nc'

    for file in files:
        count_str = formatted_count_str(json_data, count)
        with open(file) as target_file:
            line = target_file.readlines()

        line.insert(0, address_str + count_str + '\n')

        file_name = Path(file).stem

        if ext == '.*':
            extention = Path(file).suffix
        else:
            extention = ext

        out_file = target_folder + '/' + str(file_name) + extention

        with open(out_file, mode='w') as target_file:
            target_file.writelines(line)

        count += 1
    json_data.set_data(START_NUM_HISTORY ,start_num)
    messagebox.showinfo('情報', '処理が完了しました。')


def closing_func(jsonIo, root):

    main_win_size = '600x600'
    setting_win_size = '850x550'
    win_size_dict = {MAIN_WINDOW_SIZE: main_win_size, SETTING_WINDOW_SIZE:setting_win_size}
    win_hight = root.winfo_height()
    win_length = root.winfo_width()
    main_win_size = str(win_length) + 'x' + str(win_hight)
    win_size_dict[MAIN_WINDOW_SIZE] = main_win_size
    dict_win = jsonIo.get_data(WINDOW_SIZE)
    if dict_win is not None:
        if SETTING_WINDOW_SIZE in dict_win:
            win_size_dict[SETTING_WINDOW_SIZE] = dict_win.get(SETTING_WINDOW_SIZE)
    jsonIo.set_data(WINDOW_SIZE, win_size_dict)
    jsonIo.write_json()
    root.destroy()

#############################################


def get_win_size(json_data):
    if json_data.get_data(WINDOW_SIZE) is not None:
        win_size = json_data.get_data(WINDOW_SIZE)
        main_win_size = win_size.get(MAIN_WINDOW_SIZE)
    else:
        main_win_size = ('610x580')

    return main_win_size


def get_font_size(json_data):
    ''' フォントサイズをjsonから設定 '''
    if json_data.get_data(FONT_SIZE) is not None:
        font_size = json_data.get_data(FONT_SIZE)
        return font_size
    else:
        return 10


def get_path(json_data, pathtype):
    ''' 元データフォルダを設定

    履歴がオンかオフか
    デフォルトフォルダを取得するか履歴を取得するかを分岐

    引数：
    pathtype：元フォルダか先のフォルダか
    0:元フォルダ
    1:先フォルダ

    '''
    if pathtype == 0:
        def_folder = json_data.get_data(DEFAULT_FOLDER)
    else:
        def_folder = json_data.get_data(DEFAULT_POS_FOLDER)

    if def_folder is None or def_folder == '':
        history = True
    else:
        history = False

    if pathtype == 0:  # 元フォルダ
        folder_history = json_data.get_data(ORIGINAL_FOLDER_HISTORY)
    else:  # 先フォルダ
        folder_history = json_data.get_data(TARGET_FOLDER_HISTORY)

    if history:
        str_folder_initial = folder_history
    else:
        str_folder_initial = def_folder

    return str_folder_initial


def get_start_num(json_data):
    ''' スタート番号 '''
    history = json_data.get_data(BOOL_STARTNUM_HISTORY)
    # 履歴がTrue
    if history is not None and history:
        # 履歴から読む
        hist_num = json_data.get_data(START_NUM_HISTORY)
        # 履歴があれば履歴
        if hist_num is not None:
            start_num = hist_num
        else:  # 履歴がなければ1000
            start_num = '1000'
    else:  # 履歴がFalse
        defo_start_num = json_data.get_data(DEFAULT_START_NUM)
        # デフォルト開始番号を読む
        if defo_start_num is not None:
            start_num = defo_start_num
        else:
            start_num = '1000'

    return start_num
