import subprocess
import pathlib
from tkinter import messagebox as msgbox
from modules.get_mydocument import getUserDocumemntPath as getdoc

def open_explorer(path,root):
    ''' エクスプローラを開く

    '''
    doc = getdoc()
    if path:
        p_path = pathlib.Path(path)
        if p_path.exists():
            subprocess.run('explorer {}'.format(str(p_path)))
        else:
            msgbox.showinfo('情報', '入力されたフォルダが存在しません。\nマイドキュメントを表示します。', parent=root)
            
            subprocess.run('explorer {}'.format(doc))
    else:
            subprocess.run('explorer {}'.format(doc))

#if __name__ == "__main__":
   # open_explorer('C:')