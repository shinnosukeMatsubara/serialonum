import tkinter as tk
import webbrowser as wb
from classes.jsonFileIo import JsonFileIo
from modules.serialonum_const import *
from classes.wraped_button import wraped_button as btn

WEB_PAGE = r'http://www.system-i.co.jp/'
VERSION = r'1.0.0'

def linc_click(event):
    wb.open_new(WEB_PAGE)

def show_information_window(root, my_font, json_data):
    ''' プログラム情報ウィンドウを定義 '''

    information_window_root =  tk.Toplevel(root)
    information_window_root.grab_set()
    size = json_data.get_data(FONT_SIZE)
    if size is not None and size != '':
        if size == 15:
            geomet_size = ('600x200')
        else:
            geomet_size = ('350x140')
    else:
        geomet_size = ('500x250')
    information_window_root.geometry(geomet_size)
    information_window_root.geometry('+20+20')
    information_window_root.title('プログラム情報')
    #information_window_root.resizable(0,0)

    cnf = {'anchor':tk.NW, 'padx':5, 'pady':5}

    #会社情報
    campany_name_label_frame = tk.Frame(information_window_root)
    campany_name_label = tk.Label(campany_name_label_frame, text='© 2019 SYSTEM-I Inc, All rights reserved.', font=my_font)
    campany_name_label.pack(side=tk.LEFT)
    campany_name_label_frame.pack(**cnf)


    #リンク情報
    link_frame = tk.Frame(information_window_root)
    link_label = tk.Label(link_frame,text='website:', font=my_font)
    link = tk.Label(link_frame, text=WEB_PAGE, fg='blue', cursor='hand2', font=my_font)

    link_label.pack(side=tk.LEFT)
    link.pack(side=tk.LEFT)
    link.bind('<Button-1>', linc_click)
    link_frame.pack(**cnf)

    #バージョン
    version_frame = tk.Frame(information_window_root)
    version_label = tk.Label(version_frame, text='バージョン:' + VERSION, font=my_font)
    version_label.pack(side=tk.LEFT)
    version_frame.pack(**cnf)

    #閉じる
    button_cnf = {'mouse_over': '#e1ffff', 'cursor': 'hand2', 'bg':'#e1e1e1', 'relief': tk.GROOVE}
    close_button = btn(information_window_root,font=my_font, text='閉じる',command= lambda : information_window_root.destroy(), **button_cnf)
    close_button.pack(anchor=tk.NE, padx=5, pady=5)

    information_window_root.mainloop()


if __name__ == "__main__":
    root = tk.Tk()
#    show_information_window(root, my_font)
    root.mainloop()